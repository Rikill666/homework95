import React, {Component} from 'react';
import {fetchCocktails} from "../../store/actions/cocktailsActions";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import CocktailListItem from "../../components/CocktailListItem/CocktailListItem";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";

class Cocktails extends Component {
    componentDidMount() {
        if(this.props.match.params.id){
            this.props.fetchCocktails(this.props.match.params.id);
        }
        else{
            this.props.fetchCocktails();
        }

    }

    render() {
        return (
            <Grid container direction="column" spacing={2}>
                <Toolbar/>
                <Grid item container direction="row" justify="space-between" alignItems="center">
                    <Grid item>
                        <Typography variant="h4">
                            {this.props.cocktails.length > 0 ? "Cocktails" : "No Cocktails"}
                        </Typography>
                    </Grid>

                    <Grid item>
                        {
                            this.props.user ? <Button
                                color="primary"
                                component={Link}
                                to={"/cocktails/new"}
                            >
                                Add cocktail
                            </Button> : null
                        }
                    </Grid>
                </Grid>
                <Grid item container direction="row" spacing={2} justify="center">
                    {this.props.cocktails.map(cocktail => {
                        return <CocktailListItem
                            key={cocktail._id}
                            name={cocktail.name}
                            id={cocktail._id}
                            image={cocktail.image}
                            isPublished={cocktail.isPublished}
                        />
                    })}
                </Grid>
            </Grid>
        );
    }
}

const mapStateToProps = state => ({
    cocktails: state.cocktails.cocktails,
    loading: state.cocktails.loading,
    user: state.users.user,
});

const mapDispatchToProps = dispatch => ({
    fetchCocktails: (userId) => dispatch(fetchCocktails(userId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Cocktails);
