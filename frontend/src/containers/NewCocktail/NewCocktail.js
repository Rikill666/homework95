import React, {Component, Fragment} from 'react';
import CocktailForm from "../../components/CocktailForm/CocktailForm";
import {createCocktail} from "../../store/actions/cocktailsActions";
import {connect} from "react-redux";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Toolbar from "@material-ui/core/Toolbar";

class NewCocktail extends Component {

    render() {
        return (
            <Fragment>
                <Toolbar/>
                <Box pb={2} pt={2}>
                    <Typography variant="h4">New cocktail</Typography>
                </Box>

                <CocktailForm
                    error = {this.props.createError}
                    onSubmit={this.props.createCocktail}
                />
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    createError: state.cocktails.createError
});

const mapDispatchToProps = dispatch => ({
    createCocktail: cocktailData => dispatch(createCocktail(cocktailData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewCocktail);