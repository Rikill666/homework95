import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchCocktail, rateCocktail} from "../../store/actions/cocktailsActions";
import CocktailFullInfo from "../../components/CocktailFullInfo/CocktailFullInfo";
import Toolbar from "@material-ui/core/Toolbar";

class FullCocktailDetails extends Component {
    componentDidMount() {
        this.props.fetchCocktail(this.props.match.params.id);
    }
    squeakRating = () => {
        if(this.props.cocktail && this.props.user){
            const rate =  this.props.cocktail.ratings.find(r => r.user === this.props.user._id);
            if(rate){
                return rate.rating;
            }
            else{
                return 666;
            }
        }
        return 666;
    };

    render() {
        return (
            <>
                <Toolbar/>
                <CocktailFullInfo cocktail={this.props.cocktail} rate={this.props.rateCocktail} squeakRating={this.squeakRating}/>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        cocktail: state.cocktails.cocktail,
        user:state.users.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        fetchCocktail: (cocktailId) => dispatch(fetchCocktail(cocktailId)),
        rateCocktail: (cocktailId,rate) => dispatch(rateCocktail(cocktailId,rate)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(FullCocktailDetails);