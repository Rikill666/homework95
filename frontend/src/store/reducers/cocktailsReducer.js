import {
    CREATE_COCKTAIL_ERROR,
    CREATE_COCKTAIL_REQUEST,
    CREATE_COCKTAIL_SUCCESS,
    FETCH_COCKTAIL_ERROR,
    FETCH_COCKTAIL_REQUEST,
    FETCH_COCKTAIL_SUCCESS,
    FETCH_COCKTAILS_ERROR,
    FETCH_COCKTAILS_REQUEST,
    FETCH_COCKTAILS_SUCCESS,
    PUBLISH_COCKTAIL_SUCCESS, RATE_COCKTAIL_SUCCESS
} from "../actions/cocktailsActions";

const initialState = {
    cocktails: [],
    cocktail: null,
    error: null,
    createError: null,
    loading: false,
};
const cocktailsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_COCKTAILS_REQUEST:
            return {...state, loading: true};
        case FETCH_COCKTAILS_SUCCESS:
            return {...state, cocktails: action.cocktails, error: null, loading: false};
        case FETCH_COCKTAILS_ERROR:
            return {...state, error: action.error, loading: false};

        case FETCH_COCKTAIL_REQUEST:
            return {...state, loading: true};
        case FETCH_COCKTAIL_SUCCESS:
            return {...state, cocktail: action.cocktail, error: null, loading: false};
        case FETCH_COCKTAIL_ERROR:
            return {...state, error: action.error, loading: false};

        case CREATE_COCKTAIL_REQUEST:
            return {...state, loading: true};
        case CREATE_COCKTAIL_SUCCESS:
            return {...state, createError: null, loading: false};
        case CREATE_COCKTAIL_ERROR:
            return {...state, createError: action.error, loading: false};

        case PUBLISH_COCKTAIL_SUCCESS:
            const index = state.cocktails.findIndex(a => a._id === action.cocktail._id);
            let cocktails = [...state.cocktails];
            let cocktail = {...cocktails[index]};
            cocktail.isPublished = true;
            cocktails[index] = cocktail;
            return {
                ...state,
                cocktails: cocktails
            };
        case RATE_COCKTAIL_SUCCESS:

          return {...state, cocktail: action.cocktail};
        default:
            return state;
    }
};

export default cocktailsReducer;