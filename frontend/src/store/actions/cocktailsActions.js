import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const FETCH_COCKTAILS_REQUEST = 'FETCH_COCKTAILS_REQUEST';
export const FETCH_COCKTAILS_SUCCESS = 'FETCH_COCKTAILS_SUCCESS';
export const FETCH_COCKTAILS_ERROR = 'FETCH_COCKTAILS_ERROR';

export const FETCH_COCKTAIL_REQUEST = 'FETCH_COCKTAIL_REQUEST';
export const FETCH_COCKTAIL_SUCCESS = 'FETCH_COCKTAIL_SUCCESS';
export const FETCH_COCKTAIL_ERROR = 'FETCH_COCKTAIL_ERROR';

export const CREATE_COCKTAIL_REQUEST = 'CREATE_COCKTAIL_REQUEST';
export const CREATE_COCKTAIL_SUCCESS = 'CREATE_COCKTAIL_SUCCESS';
export const CREATE_COCKTAIL_ERROR = 'CREATE_COCKTAIL_ERROR';

export const DELETE_COCKTAIL_REQUEST = 'DELETE_COCKTAIL_REQUEST';
export const DELETE_COCKTAIL_SUCCESS = 'DELETE_COCKTAIL_SUCCESS';
export const DELETE_COCKTAIL_ERROR = 'DELETE_COCKTAIL_ERROR';

export const PUBLISH_COCKTAIL_REQUEST = 'PUBLISH_COCKTAIL_REQUEST';
export const PUBLISH_COCKTAIL_SUCCESS = 'PUBLISH_COCKTAIL_SUCCESS';
export const PUBLISH_COCKTAIL_ERROR = 'PUBLISH_COCKTAIL_ERROR';

export const RATE_COCKTAIL_REQUEST = 'RATE_COCKTAIL_REQUEST';
export const RATE_COCKTAIL_SUCCESS = 'RATE_COCKTAIL_SUCCESS';
export const RATE_COCKTAIL_ERROR = 'RATE_COCKTAIL_ERROR';

export const fetchCocktailRequest = () => {return {type: FETCH_COCKTAIL_REQUEST};};
export const fetchCocktailSuccess = cocktail => ({type: FETCH_COCKTAIL_SUCCESS, cocktail});
export const fetchCocktailError = (error) => {return {type: FETCH_COCKTAIL_ERROR, error};};

export const rateCocktailRequest = () => {return {type: RATE_COCKTAIL_REQUEST};};
export const rateCocktailSuccess = (cocktail) => ({type: RATE_COCKTAIL_SUCCESS, cocktail});
export const rateCocktailError = (error) => {return {type: RATE_COCKTAIL_ERROR, error};};

export const deleteCocktailRequest = () => {return {type: DELETE_COCKTAIL_REQUEST};};
export const deleteCocktailSuccess = () => ({type: DELETE_COCKTAIL_SUCCESS});
export const deleteCocktailError = (error) => {return {type: DELETE_COCKTAIL_ERROR, error};};

export const publishCocktailRequest = () => {return {type: PUBLISH_COCKTAIL_REQUEST};};
export const publishCocktailSuccess = (cocktail) => ({type: PUBLISH_COCKTAIL_SUCCESS, cocktail});
export const publishCocktailError = (error) => {return {type: PUBLISH_COCKTAIL_ERROR, error};};

export const fetchCocktailsRequest = () => {return {type: FETCH_COCKTAILS_REQUEST};};
export const fetchCocktailsSuccess = cocktails => ({type: FETCH_COCKTAILS_SUCCESS, cocktails});
export const fetchCocktailsError = (error) => {return {type: FETCH_COCKTAILS_ERROR, error};};

export const createCocktailRequest = () => {return {type: CREATE_COCKTAIL_REQUEST};};
export const createCocktailSuccess = () => ({type: CREATE_COCKTAIL_SUCCESS});
export const createCocktailError = (error) => {return {type: CREATE_COCKTAIL_ERROR, error};};

export const fetchCocktail = (cocktailId) => {
  return async dispatch => {
    try{
      dispatch(fetchCocktailRequest());
      const response = await axiosApi.get('/cocktails/'+ cocktailId);
      const cocktail = response.data;
      dispatch(fetchCocktailSuccess(cocktail));
    }
    catch (e) {
      dispatch(fetchCocktailError(e));
    }
  };
};

export const rateCocktail = (id, rate) => {
  return async dispatch => {
    try {
      dispatch(rateCocktailRequest());
      const response = await axiosApi.post('/cocktails/rate/' + id, {rating:rate});
      dispatch(rateCocktailSuccess(response.data));
    } catch (e) {
      dispatch(rateCocktailError(e));
    }
  };
};

export const deleteCocktail = (id) => {
  return async dispatch => {
    try {
      dispatch(deleteCocktailRequest());
      await axiosApi.delete('/cocktails/' + id);
      dispatch(deleteCocktailSuccess());
    } catch (e) {
      dispatch(deleteCocktailError(e));
    }
  };
};

export const publishCocktail = (id) => {
  return async dispatch => {
    try {
      dispatch(publishCocktailRequest());
      const cocktail = await axiosApi.post('/cocktails/publish/' + id);
      dispatch(publishCocktailSuccess(cocktail.data));
    } catch (e) {
      dispatch(publishCocktailError(e));
    }
  };
};

export const fetchCocktails = (userId) => {

  let url = '/cocktails';
  if(userId)
  {
    url+=('?user='+ userId);
  }
  return async dispatch => {
    try{
      dispatch(fetchCocktailsRequest());
      const response = await axiosApi.get(url);
      const cocktails = response.data;
      dispatch(fetchCocktailsSuccess(cocktails));
    }
    catch (e) {
      dispatch(fetchCocktailsError(e));
    }
  };
};
export const createCocktail = (cocktailData) => {
  return async dispatch => {
    try{
      dispatch(createCocktailRequest());
      await axiosApi.post('/cocktails', cocktailData);
      dispatch(createCocktailSuccess());
      dispatch(push('/'));
    }
    catch (error) {
      if(error.response){
        dispatch(createCocktailError(error.response.data));
      }
      else{
        dispatch(createCocktailError({global:"Network error or no internet"}));
      }
    }
  };
};


