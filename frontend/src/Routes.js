import React from 'react';
import {useSelector} from "react-redux";
import {Redirect, Route, Switch} from "react-router-dom";
import Cocktails from "./containers/Cocktails/Cocktails";
import Login from "./containers/Login/Login";
import NewCocktail from "./containers/NewCocktail/NewCocktail";
import FullCocktailDetails from "./containers/FullCocktailDetails/FullCocktailDetails";


const ProtectedRoute = ({isAllowed, ...props}) => (
  isAllowed ? <Route {...props}/> : <Redirect to="/login"/>
);

const Routes = () => {
  const user = useSelector(state => state.users.user);

  return (
    <Switch>
      <Route path="/" exact component={Cocktails} />
      <Route path="/cocktails" exact component={Cocktails} />
      <Route path="/login" exact component={Login} />
      <ProtectedRoute isAllowed={user} path="/cocktails/new" exact component={NewCocktail} />
      <ProtectedRoute isAllowed={user} path="/cocktails/:id" exact component={FullCocktailDetails} />
      <ProtectedRoute isAllowed={user} path="/users/:id" exact component={Cocktails} />
    </Switch>
  );
};

export default Routes;