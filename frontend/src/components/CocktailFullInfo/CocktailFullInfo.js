import React, {useEffect} from 'react';
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {Card} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {apiURL} from "../../constants";
import imageNotAvailable from "../../assets/images/image_not_available.jpg";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Rating from "@material-ui/lab/Rating";

const useStyles = makeStyles({
    root: {
        textAlign: "center",
        marginTop: "50px"
    },
    details: {
        display: 'flex',
        flexDirection: 'column',
    },
    cover: {
        width: "40%",
        marginLeft: "30%",

    },
    content: {
        flex: '1 0 auto',
    },
});

const CocktailFullInfo = (props) => {
    const [value, setValue] = React.useState( 0);
    useEffect(() => {
        if(props.squeakRating()){
            if(props.squeakRating() === 666){
                setValue(0)
            }
            else{
                setValue(props.squeakRating())
            }
        }
    }, [props.squeakRating()]);
    let image = imageNotAvailable;
    if (props.cocktail && props.cocktail.image) {
        image = apiURL + '/' + props.cocktail.image.replace('\\', '/');
    }
    const classes = useStyles();

    const ratingCalculation = () => {
        return props.cocktail.ratings.reduce((acc, rate) => {
            acc += rate.rating;
            return acc
        }, 0)/props.cocktail.ratings.length;
    };
    return props.cocktail ?
        <Card>
            <Grid container direction="row">
                <Grid item className={classes.root} xs={12} md={6} lg={4}>
                    <img src={image} alt={props.cocktail.name}/>
                </Grid>
                <Grid item xs={12} md={6} lg={8}>
                    <div className={classes.details}>
                        <CardContent className={classes.content}>
                            <Typography variant="h3">
                                {props.cocktail.name}
                            </Typography>
                            {props.cocktail.ratings.length > 0 ?
                                <Typography variant="h5">
                                    Rating: {ratingCalculation() + " (" + props.cocktail.ratings.length + " votes)"}
                                </Typography> :
                                <Typography variant="h5">
                                    The cocktail has not yet been rated
                                </Typography>
                            }

                            <Typography variant="h6">
                                Ingredients:
                            </Typography>
                            <List>
                                {props.cocktail.ingredients.map(c => {
                                    return (
                                        <ListItem key={c.name}>
                                            <ListItemIcon>
                                                <FiberManualRecordIcon/>
                                            </ListItemIcon>
                                            <ListItemText primary={c.name + " - " + c.count}/>
                                        </ListItem>)
                                })}
                            </List>
                        </CardContent>
                    </div>
                </Grid>
            </Grid>
            <CardContent>
                <Typography variant="h6">
                    Recipe:
                </Typography>
                <Typography paragraph>
                    {props.cocktail.recipe}
                </Typography>
            </CardContent>
            <Box component="fieldset" mb={3} borderColor="transparent">
                <Typography component="legend">Rate: </Typography>
                <Rating
                    name="simple-controlled"
                    value={value}
                    size="large"
                    onChange={(event, newValue) => {
                        setValue(newValue);
                        props.rate(props.cocktail._id, newValue);
                    }}
                />
            </Box>
        </Card> : null
};

export default CocktailFullInfo;