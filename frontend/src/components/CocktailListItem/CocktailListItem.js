import React from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import {Card} from "@material-ui/core";
import CardMedia from "@material-ui/core/CardMedia";
import {makeStyles} from "@material-ui/core/styles";
import imageNotAvailable from "../../assets/images/image_not_available.jpg";
import {apiURL} from "../../constants";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CloseIcon from '@material-ui/icons/Close';
import CardHeader from "@material-ui/core/CardHeader";
import IconButton from "@material-ui/core/IconButton";
import PublishIcon from '@material-ui/icons/Publish';
import {useDispatch, useSelector} from "react-redux";
import {deleteCocktail, fetchCocktails, publishCocktail} from "../../store/actions/cocktailsActions";
import {Link} from "react-router-dom";

const useStyles = makeStyles({
    card: {
        width: "70%",
        marginLeft: "15%"
    },
    media: {
        paddingTop: '100%', // 16:9
    },
    header: {
        display: 'block',
    },
    text: {
        textDecoration: "none",
        color: "black"
    }
});

const CocktailListItem = props => {
    const user = useSelector(state => state.users.user);
    const dispatch = useDispatch();
    const classes = useStyles();

    const cocktailDelete = (id) => {
        dispatch(deleteCocktail(id));
        dispatch(fetchCocktails());
    };
    const cocktailPublish = (id) => {
        dispatch(publishCocktail(id));
        dispatch(fetchCocktails());
    };

    let image = imageNotAvailable;

    if (props.image) {
        image = apiURL + '/' + props.image.replace('\\', '/');
    }

    return (
        <Grid item xs={12} sm={6} md={4}>
            <Card className={classes.card}>
                {user && user.role === "admin" ?
                    <CardHeader className={classes.header}
                                action={
                                    <div style={{
                                        display: "flex",
                                        justifyContent: "space-between"
                                    }}>
                                        {!props.isPublished ?
                                            <IconButton
                                                onClick={() => cocktailPublish(props.id)}
                                                aria-label="settings">
                                                <PublishIcon color={'action'}
                                                             fontSize={'large'}/>
                                            </IconButton> : <div/>
                                        }
                                        <IconButton
                                            onClick={() => cocktailDelete(props.id)}
                                            aria-label="settings">
                                            <CloseIcon color={'action'}
                                                       fontSize={'large'}/>
                                        </IconButton>
                                    </div>
                                }
                    /> : null}
                <Link to={"/cocktails/" + props.id} className={classes.text}>
                    <CardMedia image={image} title={props.name} className={classes.media}/>
                    <CardContent>
                        <Typography variant="h5">
                            {props.name}
                        </Typography>
                        {!props.isPublished ?
                            <Typography variant="h6">
                                No public
                            </Typography> : null}

                    </CardContent>
                </Link>
            </Card>
        </Grid>
    );
};

CocktailListItem.propTypes = {
    image: PropTypes.string,
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    isPublished: PropTypes.bool.isRequired,
};

export default CocktailListItem;