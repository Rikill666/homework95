import React, {Component} from 'react';
import FormElement from "../UI/Form/FormElement";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import ClearIcon from '@material-ui/icons/Clear';
import IconButton from "@material-ui/core/IconButton";

class CocktailForm extends Component {
    state = {
        name: '',
        image: '',
        recipe:'',
        ingredients:[]
    };

    addIngredient = () => {
        this.setState({ingredients:[...this.state.ingredients, {name:"", count:""}]})
    };
    changeIngredient =(i, key , e) =>{
        const ingCopy = {...this.state.ingredients[i]};
        ingCopy[key] = e.target.value;

        const ingredients = [...this.state.ingredients];
        ingredients[i] = ingCopy;
        this.setState({ingredients});
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            let value = this.state[key];
            if (key === 'ingredients') {
                value = JSON.stringify(value);
            }
            formData.append(key, value);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };


    getFieldError = fieldName => {
        try {
            return this.props.error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    deleteIngredient =(index)=>{
        const ingredients = [...this.state.ingredients];
        ingredients.splice(index, 1);
        this.setState({ingredients});
    };

    render() {
        return (
            <form onSubmit={this.submitFormHandler}>
                <Grid container direction="column" spacing={2}>
                    <Grid item xs>
                        <FormElement
                            type="text"
                            propertyName="name"
                            title="Name"
                            placeholder="Enter name cocktail"
                            onChange={this.inputChangeHandler}
                            value={this.state.name}
                            error={this.getFieldError('name')}
                        />
                    </Grid>
                    {this.state.ingredients.map((ing, i)=>(
                        <Grid key={i} item xs container direction="row" spacing={3}>
                            <Grid item xs={12} md={6} lg={8}>
                                <FormElement
                                    type="text"
                                    propertyName="name"
                                    title="Ingredient name"
                                    placeholder="Enter ingredient name"
                                    onChange={e => this.changeIngredient(i,"name",e)}
                                    value={ing.name}
                                    error={this.getFieldError('ingredients.'+i+'.name')}
                                />
                            </Grid>
                            <Grid item xs={11} md={5} lg={3}>
                                <FormElement
                                    type="text"
                                    propertyName="count"
                                    title="Ingredient count"
                                    placeholder="Enter ingredient count"
                                    onChange={e => this.changeIngredient(i,"count",e)}
                                    value={ing.count}
                                    error={this.getFieldError('ingredients.'+i+'.count')}
                                />
                            </Grid>
                            <Grid item xs={1}>
                                <IconButton onClick={()=>this.deleteIngredient(i)} color="inherit" >
                                        <ClearIcon/>
                                </IconButton>
                            </Grid>
                        </Grid>
                    ))}
                    <Grid item xs>
                        <Button onClick={this.addIngredient} color="primary" variant="contained">Add ingredient</Button>
                    </Grid>
                    <Grid item xs>
                        <FormElement
                            type="text"
                            propertyName="recipe"
                            title="Recipe"
                            placeholder="Enter recipe"
                            onChange={this.inputChangeHandler}
                            value={this.state.recipe}
                            error={this.getFieldError('recipe')}
                        />
                    </Grid>
                    <Grid item xs>
                        <FormElement
                            type="file"
                            propertyName="image"
                            title="Image"
                            onChange={this.fileChangeHandler}
                            error={this.getFieldError('image')}
                        />
                    </Grid>
                    <Grid item xs>
                        <Button type="submit" color="primary" variant="contained">Save</Button>
                    </Grid>
                </Grid>
            </form>
        );
    }
}

export default CocktailForm;