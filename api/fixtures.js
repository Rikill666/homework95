const mongoose = require('mongoose');
const config = require('./config');
const Cocktail = require('./models/Cocktail');
const User = require('./models/User');
const nanoid = require("nanoid");

const run = async () => {
  await mongoose.connect(config.database, config.databaseOptions);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (let coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, admin] = await User.create({
    facebookId: 'user',
    password: '123',
    token: nanoid(),
    displayName:"User"
  }, {
    facebookId: 'admin',
    password: '123',
    role: 'admin',
    token: nanoid(),
    avatar: 'avatar/fixtures/admin.jpg',
    displayName:"Admin"
  });


  await Cocktail.create({
    user: user,
    recipe: "Put a slice of orange and cocktail cherry in the Rocks\n" +
        "Add a cane sugar cube soaked with Angostura bitter 1 dash and crush madler\n" +
        "Fill Rocks with Ice Cubes\n" +
        "Pour bourbon 50 ml and gently mix with a cocktail spoon",
    ingredients: [{name:"Bourbon", count:"50 ml"},{name:"Orange", count:"40 gram"},{name:"Sugar", count:"5 gram"},{name:"Red cherry", count:"5 gram"},{name:"Angostura Bitter", count:"1 ml"},{name:"Ice", count:"120 gram"}],
    ratings:[{user:user, rating: 5}],
    name: "Old fashion",
    image: 'uploads/fixtures/oldFashion.jpg'
  }, {
    user: user,
    recipe: "Make a salt edging on the margarita glass\n" +
        "Pour lime juice 30 ml, sugar syrup 10 ml, triple sec 25 ml liquor and 50 ml silver tequila into the shaker\n" +
        "Fill the shaker with ice cubes and whisk\n" +
        "Pour over the strainer into a chilled margarita glass\n" +
        "Garnish with lime",
    ingredients: [{name:"Silver tequila", count:"50 ml"},{name:"Triple sec liquor", count:"25 ml"},{name:"Sugar syrup", count:"10 ml"},{name:"Lime juice", count:"30 ml"},{name:"Salt", count:"2 gram"},{name:"Ice", count:"200 gram"}],
    ratings:[],
    name: "Margarita",
    image: 'uploads/fixtures/margarita.jpg',
    isPublished: true
  }, {
    user: user,
    recipe: "Fill the rox with ice cubes to the top\n" +
        "Pour non-fat cream 30 ml, coffee liquor 30 ml and vodka 30 ml into a glass\n" +
        "Stir with a cocktail spoon until the walls freeze",
    ingredients: [{name:"Vodka", count:"30 ml"},{name:"Nonfat cream", count:"30 ml"},{name:"Coffee liqueur", count:"30 ml"},{name:"Ice", count:"120 gram"}],
    ratings:[{user:user, rating: 3}],
    name: "White Russian",
    image: 'uploads/fixtures/whiteRussian.jpg'
  });

  mongoose.connection.close();
};

run().catch(e => {
  mongoose.connection.close();
  throw e;
});