const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const ratingSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
    rating: {
        type: Number,
        min: 1,
        max: 5
    }
});
const ingredientSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  count: {
    type: String,
    required: true
  }
});
const CocktailSchema = new Schema({
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    recipe: {
        type: String,
        required: true
    },
    isPublished: {
        type: Boolean,
        required: true,
        default: false
    },
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    ingredients: [ingredientSchema],
    ratings: [ratingSchema]
});

const Cocktail = mongoose.model('Cocktail', CocktailSchema);

module.exports = Cocktail;