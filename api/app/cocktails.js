const express = require('express');
const ValidationError = require('mongoose').Error.ValidationError;
const permit = require('../middleware/permit');
const auth = require('../middleware/auth');
const upload = require('../multer').uploads;
const User = require('../models/User');
const Cocktail = require('../models/Cocktail');

const router = express.Router();

router.get('/', async (req, res) => {
    let user = null;
    let cocktails;
    const authorizationHeader = req.get('Authorization');
    if(authorizationHeader){
        const [type, token] = authorizationHeader.split(' ');
        if(type === 'Token' || token){
            const regUser = await User.findOne({token});
            if(regUser){
                user = regUser;
            }
        }
    }
    if (req.query.user && user) {
        cocktails = await Cocktail.find({user: req.query.user});
    }
    else if(user && user.role === "admin"){
        cocktails = await Cocktail.find();
    }
    else{
        cocktails = await Cocktail.find({isPublished: true});
    }
    res.send(cocktails);
});


router.post('/', [auth, upload.single('image')], async (req, res) => {
    try {
        const user = req.user;
        const cocktailData = {
            recipe: req.body.recipe,
            user: user,
            name:req.body.name,
            ingredients: JSON.parse(req.body.ingredients),
            ratings: [],
        };
        if (req.file) {
            cocktailData.image = req.file.filename;
        }
        const cocktail = new Cocktail(cocktailData);
        await cocktail.save();
        return res.send({id: cocktail._id});
    } catch (e) {
        if (e instanceof ValidationError) {
            return res.status(400).send(e);
        } else {
            console.log(e);
            return res.sendStatus(500);
        }
    }
});

router.get("/:id", async (req, res) => {
    const id = req.params.id;
    if(id){
        try {
            const cocktail = await Cocktail.findOne({_id: id});
            if (!cocktail) {
                return res.status(404).send({message: "Cocktail not found"});
            }
            return res.send(cocktail);
        }
        catch(error){
            return res.status(400).send(error);
        }
    }
});

router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const cocktail = await Cocktail.findOne({_id: id});
        if(!cocktail){
            return res.status(404).send("No this cocktail");
        }
        await Cocktail.deleteOne({_id: id});
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/publish/:id',[auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const cocktail = await Cocktail.findOne({_id: id});
        cocktail.isPublished = true;
        await cocktail.save();
        res.send(cocktail);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/rate/:id',auth , async (req, res) => {
    const id = req.params.id;
    const user = req.user;
    try {
        const cocktail = await Cocktail.findOne({_id: id});
        const rateIndex = cocktail.ratings.findIndex(r => r.user.toString() === user._id.toString());
        if(rateIndex < 0){
            const newRating = {
                user:user,
                rating: req.body.rating,
            };
            cocktail.ratings.push(newRating);
        }
        else{
            cocktail.ratings[rateIndex].rating = req.body.rating;
        }
        await cocktail.save();
        res.send(cocktail);
    } catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;